﻿namespace MindBucks.Data.ViewModels
{
    public class StrikeDashboardViewModel
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public int Strikes { get; set; }
        public int Cash { get; set; }
    }
}