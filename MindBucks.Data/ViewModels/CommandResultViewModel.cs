﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MindBucks.Data.ViewModels
{
    public class CommandResultViewModel
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }

        public static CommandResultViewModel CreateSuccess()
        {
            return new CommandResultViewModel {Success = true};
        }

        public static CommandResultViewModel CreateError(string errorMessage)
        {
            return new CommandResultViewModel { Success = false, ErrorMessage = errorMessage};
        }
    }
}
