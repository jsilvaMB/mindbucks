﻿using System.ComponentModel.DataAnnotations;

namespace MindBucks.Data.ViewModels.CashIn
{
    public class CashInViewModel
    {
        public int Available { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Ammount must be bigger than {1}")]
        public int Ammount { get; set; }
    }
}