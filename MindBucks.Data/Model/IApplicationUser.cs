﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MindBucks.Data.Model
{
    public interface IApplicationUser
    {
        string DisplayName { get; set; }
        string UserName { get; set; }
        string Email { get; set; }
    }
}
