﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MindBucks.Data.ViewModels.CashIn;
using ShortBus;

namespace MindBucks.Data.Queries
{
    public class StrikesAvailableForCashQuery : IRequest<CashInViewModel>
    {
        public string Email { get; set; }
    }
}
