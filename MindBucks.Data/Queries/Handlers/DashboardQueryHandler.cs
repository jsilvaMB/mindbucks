﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MindBucks.Data.Model;
using MindBucks.Data.ViewModels;
using ShortBus;

namespace MindBucks.Data.Queries.Handlers
{
    public class DashboardQueryHandler : IRequestHandler<DashboardQuery, DashboardViewModel>
    {
        private readonly IMediator _mediator;
        private readonly IDatabaseContext _dbContext;

        public DashboardQueryHandler(IMediator mediator, IDatabaseContext dbContext)
        {
            _mediator = mediator;
            _dbContext = dbContext;
        }

        public DashboardViewModel Handle(DashboardQuery request)
        {
            var strikesRequest = _mediator.Request(new StrikesPerUserQuery());
            var strikes = strikesRequest.Data;
            var users = _dbContext.Users.FindAllAs<ApplicationUser>();

            var allStrikes = from u in users
                             join leftStrikes in strikes on u.Email equals leftStrikes.Email into tempStrikes
                             from s in tempStrikes.DefaultIfEmpty()
                             select new StrikeDashboardViewModel
                             {
                                 Cash = s != null ? s.Cash : 0,
                                 Strikes = s != null ? s.Strikes : 0,
                                 Email = u.Email,
                                 Name = u.DisplayName,
                             };

            var dashboarViewModel = new DashboardViewModel
            {
                Strikes = allStrikes.OrderByDescending(x => x.Cash + x.Strikes)
            };

            return dashboarViewModel;
        }
    }
}
