using System;
using MindBucks.Data.Model;
using MongoDB.Driver;

namespace MindBucks.Data
{
    public interface IDatabaseContext : IDisposable
    {
        MongoCollection<Strike> Strikes { get; }
        MongoCollection Users { get; }
    }
}