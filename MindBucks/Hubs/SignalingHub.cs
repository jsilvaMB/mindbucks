﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace MindBucks.Hubs
{
    public class SignalingHub : Hub
    {

        public static void RefreshClientsDashboard()
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<SignalingHub>();
            hubContext.Clients.All.refreshDashboard();
        }
    }
}