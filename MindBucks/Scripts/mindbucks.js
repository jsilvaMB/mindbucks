﻿
function initActionButton(button) {
    $(button).click(function() {
        $(button).prop("disabled", true);
        var action = $(button).data("action");
        console.log('calling action: ' + action);
        $.post(action).success(function () {
            $(button).prop("disabled", false);
            console.log('action called with success');
        }).error(function () {
            $(button).prop("disabled", false);
            alert('error calling action');
        });
    });
}

function initDashboard() {
    // Reference the auto-generated proxy for the hub.
    var chat = $.connection.signalingHub;

    chat.client.refreshDashboard = function () {
        //reload dashboard and re-wire action buttons
        $('#dashboard').load($('#dashboard').data("url"), function () {
            $("button.action", $('#dashboard')).each(function(index, button) {
                initActionButton(button);
            });
        });
    };

    $.connection.hub.start().done(function () {
        console.log("hub started");
    });
}

$(function () {
    $("button.action").each(function (index, button) {
        initActionButton(button);
    });
});