﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using Autofac.Features.Variance;
using Autofac.Integration.Mvc;
using MindBucks.Data;
using ShortBus;
using DependencyResolver = System.Web.Mvc.DependencyResolver;

namespace MindBucks
{
    public class IoCConfig
    {

        /// <summary>
        /// For more info see 
        /// :https://code.google.com/p/autofac/wiki/MvcIntegration (mvc4 instructions)
        /// </summary>
        public static void RegisterDependencies()
        {
            #region Create the builder
            var builder = new ContainerBuilder();
            #endregion

            #region Setup a common pattern
            // placed here before RegisterControllers as last one wins
            builder.RegisterAssemblyTypes()
                   .Where(t => t.Name.EndsWith("Repository"))
                   .AsImplementedInterfaces()
                   .InstancePerHttpRequest();
            builder.RegisterAssemblyTypes()
                   .Where(t => t.Name.EndsWith("Service"))
                   .AsImplementedInterfaces()
                   .InstancePerHttpRequest();
            #endregion

            #region Register all controllers for the assembly
            // Note that ASP.NET MVC requests controllers by their concrete types, 
            // so registering them As<IController>() is incorrect. 
            // Also, if you register controllers manually and choose to specify 
            // lifetimes, you must register them as InstancePerDependency() or 
            // InstancePerHttpRequest() - ASP.NET MVC will throw an exception if 
            // you try to reuse a controller instance for multiple requests. 
            builder.RegisterControllers(typeof(MvcApplication).Assembly)
                   .InstancePerDependency();

            #endregion

            #region Setup of Mediator
            // this is needed to allow the Mediator to resolve contravariant handlers (not enabled by default in Autofac)
            builder.RegisterSource(new ContravariantRegistrationSource());

            builder.RegisterAssemblyTypes(typeof(IMediator).Assembly, typeof(MvcApplication).Assembly)
                .AsClosedTypesOf(typeof(IRequestHandler<,>))
                .AsImplementedInterfaces();

            builder.RegisterType<Mediator>().AsImplementedInterfaces().InstancePerLifetimeScope();

            // to allow ShortBus to resolve lifetime-scoped dependencies properly, 
            // we really can't use the default approach of setting the static (global) dependency resolver, 
            // since that resolves instances from the root scope passed into it, rather than 
            // the current lifetime scope at the time of resolution.  
            // Resolving from the root scope can cause resource leaks, or in the case of components with a 
            // specific scope affinity (AutofacWebRequest, for example) it would fail outright, 
            // since that scope doesn't exist at the root level.
            builder.RegisterType<ShortBus.Autofac.AutofacDependencyResolver>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            #endregion

            #region Register modules
            builder.RegisterAssemblyModules(typeof(MvcApplication).Assembly, typeof(IocModule).Assembly);
            #endregion

            #region Model binder providers - excluded - not sure if need
            //builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            //builder.RegisterModelBinderProvider();
            #endregion

            #region Inject HTTP Abstractions
            /*
         The MVC Integration includes an Autofac module that will add HTTP request 
         lifetime scoped registrations for the HTTP abstraction classes. The 
         following abstract classes are included: 
        -- HttpContextBase 
        -- HttpRequestBase 
        -- HttpResponseBase 
        -- HttpServerUtilityBase 
        -- HttpSessionStateBase 
        -- HttpApplicationStateBase 
        -- HttpBrowserCapabilitiesBase 
        -- HttpCachePolicyBase 
        -- VirtualPathProvider 

        To use these abstractions add the AutofacWebTypesModule to the container 
        using the standard RegisterModule method. 
        */
            builder.RegisterModule<AutofacWebTypesModule>();

            #endregion

            #region Set the MVC dependency resolver to use Autofac
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            //ShortBus.DependencyResolver.SetResolver(new ShortBus.Autofac.AutofacDependencyResolver(container));
            #endregion

        }

    }
}