﻿using Owin;

namespace MindBucks
{
	public partial class Startup
	{
	    void ConfigureSignalr(IAppBuilder app)
	    {
            app.MapSignalR();
	    }
	}
}