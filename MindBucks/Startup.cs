﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MindBucks.Startup))]
namespace MindBucks
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureSignalr(app);
        }
    }
}
