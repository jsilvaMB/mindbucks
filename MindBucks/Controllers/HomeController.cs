﻿using System.Web.Mvc;
using Microsoft.AspNet.SignalR;
using MindBucks.Data.Queries;
using MindBucks.Helpers;
using MindBucks.Hubs;
using ShortBus;

namespace MindBucks.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMediator _mediator;

        public HomeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            var dashboarViewModel = _mediator.Request(new DashboardQuery()).Data;
            return PartialView(dashboarViewModel);
        }

        [HttpPost]
        public ActionResult Strike(string email)
        {
            var userEmail = User.Identity.GetEmail();
            var displayName = User.Identity.GetDisplayName();
            var result = _mediator.Request(new StrikeCommand
            {
                Email = email,
                RequesterEmail = userEmail,
                RequesterDisplayName = displayName
            });

            if (!result.Data.Success)
            {
                //handle error
                return RedirectToAction("Index");
            }

            SignalingHub.RefreshClientsDashboard();
            return Content(string.Empty);
        }



    }
}